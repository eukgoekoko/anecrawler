(ns anecrawler.core
  (:use hickory.core)
  (:require [clj-http.client :as client]
            [hickory.select :as s])
  (:gen-class))

(defn -main
  "Web-crawler for anekdot.ru"
  [& args]
  (run! println
    (map :content (s/select (s/child (s/class "text"))
      (as-hickory
        (parse 
          (:body
            (client/get "https://www.anekdot.ru/"))))))))
